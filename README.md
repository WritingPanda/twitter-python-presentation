# Twitter Python Experiments and Discoveries

This presentation is more about problem solving than it is about how to do something cool with python and Twitter.

I will be talking about what I did to meet specific requirements for our project at work.

General outline:

1. Introduction of topic
2. Introduction of speaker
3. What is the objective of the project?
4. What are the requirements for the project?
5. What were the initial plans and thoughts on how to meet the requirements?
6. Twitter Libraries
	- Tweepy
	- Twitterscraper
7. Tweepy
	- What is it?
	- How to use it?
	- Overall review
	- Limitations
8. Twitterscraper
	- What is it?
	- How to use it?
	- Overall review
	- Limitations
9. How I used both of them for the project to meet the requirements in two different ways
10. How you can get started analyzing Twitter data
	- Why do you want to analyze Twitter data?
	- What do you hope to learn?
	- Which library would work for different use cases?

Alamo City Python group presentation will be (or was) on March 5, 2020, in the basement of Geekdom. The honorable Doug M. is (was) presiding. 

